<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
  // Injecter user dans route dashboard ???
  // /{user} $user

  /**
   * @Route("/dashboard", name="dashboard")
   */
  public function indexAction()
  {
    return $this->render('backend/index.html.twig');
  }
}
