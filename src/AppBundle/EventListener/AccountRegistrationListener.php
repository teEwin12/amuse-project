<?php

// src/AppBundle/EventListener/AccountRegistrationListener.php

namespace AppBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Model\UserManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityManager;

class AccountRegistrationListener implements EventSubscriberInterface
{
  private $um;

  public function __construct(UserManager $um)
  {
    $this->um = $um;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents()
  {
    return array(
      FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
    );
  }

  public function onRegistrationSuccess(FormEvent $event)
  {
    $user = $event->getForm()->getData();
    $uType = $user->getType();
    // Récup role lié au type user -> ici ou dans controller registration ???
    $user->addRole('ROLE_AMUSE');

    $this->um->updateUser($user);
  }
}
