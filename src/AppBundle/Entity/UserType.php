<?php

// src/AppBundle/Entity/UserType.php

namespace AppBundle\Entity;

class UserType
{
  const ADMIN = 1;
  const ILLUSTRATOR = 2;
}
