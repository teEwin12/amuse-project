<?php
// src/AppBundle/Form/Type/RegistrationFormType.php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // add your custom field
        //$builder->add('name');
        $builder->add('type','choice', array(
            'choices' => array(1 => 'Illustrator',2 => 'Scenarist')
          )
        );
    }

    public function getParent()
    {
        return 'fos_user_registration';
    }

    public function getName()
    {
        return 'amuse_user_registration';
    }
}
